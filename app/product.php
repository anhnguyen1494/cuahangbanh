<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $table = "products";

    public function product_type(){
    	return $this->belongsTo('App\producttype','id_type','id');
    }
    public function bill_detail(){
    	return $this->hasMany('App\billdetail','id_product','id');
    }
}
