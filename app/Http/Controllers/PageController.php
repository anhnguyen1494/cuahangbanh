<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slide;
use App\product;
use App\producttype;
use App\Cart;
use App\customer;
use App\bill;
use App\billdetail;
use App\User;
use Session;
use Hash;
use Validate;
use Auth;

class PageController extends Controller
{
    public function getIndex(){
        $slide = slide::all(); 
        $new_product = product::where('new',1)->paginate(4); 
        $promotion_product= product::where('promotion_price','<>',0)->paginate(8);
        $loai = producttype::all();
        return view('page.trangchu',compact('slide','new_product','promotion_product','loai'));
    }

    public function getLoaiSp($type){
        $sp_theoloai = product::where('id_type',$type)->get();
        $sp_khac =product::where('id_type','<>',$type)->paginate(3);
        $loai =producttype::all();
        $loai_sp=producttype::where('id',$type)->first();
    	return view('page.loai_sanpham',compact('sp_theoloai','sp_khac','loai', 'loai_sp'));
    }

    public function getChitiet(Request $req){
        $sanpham = product::where('id',$req->id)->first();
        $sp_tuongtu = product::where('id_type',$sanpham->id_type)->where('id','<>',$sanpham->id)->paginate(3);
    	return view('page.chitiet_sanpham', compact('sanpham','sp_tuongtu'));
    }

    public function getLienhe(){
    	return view('page.lienhe');
    }

    public function getGioithieu(){
    	return view('page.gioithieu');
    }

    public function getAddtoCart(Request $req,$id){
        $product = product::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->add($product,$id);
        $req->session()->put('cart',$cart);
        return redirect()->back();
    }

    public function getDelItemCart(Request $req,$id){
        $oldCart = Session::has('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        
        if (count($cart->items)>0) {
            $req->session()->put('cart',$cart);
        }
        else Session::forget('cart');
        return redirect()->back();
    }

    public function getCheckout(){
        if (Session::has('cart')) {
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            return view('page.dat_hang',['product_cart'=>$cart->items,'totalPrice'=>$cart->totalPrice,'totalQty'=>$cart->totalQty]);
        }
        else{
            $cart = new Cart(null);
            return view('page.dat_hang',['product_cart'=>$cart->items,'totalPrice'=>$cart->totalPrice,'totalQty'=>$cart->totalQty]);
        }
    }

    public function postCheckout(Request $req){
        $cart= Session::get('cart');
        $customer = new customer;
        $customer->name = $req->full_name;
        $customer->gender= $req->gender;
        $customer->email = $req->email;
        $customer->phone_number = $req->phone;
        $customer->address = $req->address;
        $customer->note= $req->note;
        $customer->save();

        $bill = new bill;
        $bill->id_customer = $customer->id;
        $bill->date_order = date('Y-m-d');
        $bill->total = $cart->totalPrice;
        $bill->payment= $req->payment_method;
        $bill->note=$req->note;
        $bill->save();

        
        foreach($cart->items as $key=>$value)
        {
            $billdetail = new billdetail;
            $billdetail->id_bill = $bill->id;
            $billdetail->id_product = $key;
            $billdetail->quantity = $value['qty'];
            $billdetail->unit_price = $value['price']/$value['qty'];
            $billdetail->save();
        }

        Session::forget('cart');
        return redirect()->back()->with('thongbao','Đặt hàng thành công');
    }

    public function getLogin(){
        return view('page.login');
    }

    public function postLogin(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email',
                'password'=>'required|min:8'
            ],
            [
                'email.required' =>'Nhập email đi nào!',
                'email.email'=>'Nhập cho đúng định dạng email nào!',
                'password.required'=>'quên điền password kìa!',
                'password.min'=>'password phải có ít nhất 8 kí tự nhé!',
            ]);
        $credentials = array('email'=>$req->email, 'password'=>$req->password);
        if (Auth::attempt($credentials)) {
            return redirect()->back()->with(['flag'=>'success', 'message'=>'Đăng nhập thành công']);
        }
        else return redirect()->back()->with(['flag'=>'danger', 'message'=>'Đăng nhập thất bại']);
    }

    public function getSignup(){
        return view('page.signup');
    }

    public function postSignup(Request $req){
        $this->validate($req,
            [
                'email'=>'required|email|unique:users,email',
                'fullname'=>'required',
                'address'=>'required',
                'phone' =>'required|regex:/[0-9]{10}/',
                'password'=>'required|min:8',
                're-password'=>'required|same:password'
            ],
            [
                'email.required' =>'Nhập email đi nào!',
                'email.email'=>'Nhập cho đúng định dạng email nào!',
                'email.unique'=>'Email này đã tồn tại rồi nha!',
                'fullname.required'=>'Chưa nhập họ tên kìa!',
                'address.required'=>'Chưa nhập địa chỉ kìa!',
                'phone.required'=>'quên điền số điện thoại rồi kìa!',
                'phone.regex'=>'Số điện thoại phải 10 số nhé!',
                'password.required'=>'quên điền password kìa!',
                'password.min'=>'password phải có ít nhất 8 kí tự nhé!',
                're-password.required'=>'quên nhập lại password kìa!',
                're-password.same'=>'password nhập lại không đúng nhé'
            ]

        );
        $user = new User();
        $user->full_name = $req->fullname;
        $user->email=$req->email;
        $user->password = Hash::make($req->password);
        $user->phone =$req->phone;
        $user->address=$req->address;
        $user->save();

        return redirect()->back()->with('thanhcong','Tạo tài khoản thành công');
    }

    public function getLogout(){
        Auth::logout();
        return redirect()->route('trang-chu');
    }

    public function getSearch(Request $req){
        $product = product::where('name','like','%'.$req->key.'%')
                    ->orwhere('unit_price','like','%'.$req->key.'%')->get();

        return view('page.search',compact('product'));
    }
}
